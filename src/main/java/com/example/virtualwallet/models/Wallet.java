package com.example.virtualwallet.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "wallets")
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wallet_id")
    @NotNull
    private int id;

    @Column(name = "user_id")
    @NotNull
    private int userId;

    @Column(name = "amount")
    @NotNull
    private double amount;

    @JsonIgnore
    @OneToMany(mappedBy = "wallet")
    private List<Card> cardList;


    public Wallet() {
        this.cardList = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public List<Card> getCardList() {
        return cardList;
    }

    public void setCardList(List<Card> cardSet) {
        this.cardList = cardSet;
    }

}
