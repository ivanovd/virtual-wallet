package com.example.virtualwallet.services.impl;

import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.InvalidUserInputException;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.UserVerification;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.UserRepository;
import com.example.virtualwallet.repositories.UserVerificationRepository;
import com.example.virtualwallet.repositories.WalletRepository;
import com.example.virtualwallet.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository ;
    private WalletRepository walletRepository;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private UserVerificationRepository userVerificationRepository;
    private EmailSenderService emailSenderService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           WalletRepository walletRepository,
                           UserDetailsManager userDetailsManager,
                           PasswordEncoder passwordEncoder,
                           UserVerificationRepository userVerificationRepository,
                           EmailSenderService emailSenderService
                           ) {
        this.userRepository = userRepository;
        this.walletRepository = walletRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.userVerificationRepository = userVerificationRepository;
        this.emailSenderService = emailSenderService;
    }

    @Override
    public void create(int id,User user, MultipartFile multipartFile) {
        if(userRepository.existsByUsername(user.getUsername())){
            throw new DuplicateEntityException(String.format(
                    "User with username %s already exists!",user.getUsername()));
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User userDetails =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        passwordEncoder.encode(user.getPassword()),
                        authorities);

        userDetailsManager.createUser(userDetails);
        User newUser = userRepository.getByUsername(user.getUsername()).get(0);

        String path = uploadFile(multipartFile);
        user.setProfilePicture(path);

        Wallet wallet = new Wallet();
        wallet.setUserId(newUser.getId());
        walletRepository.save(wallet);
        newUser.setWallet(wallet);

        update(newUser.getId(),user,multipartFile, null);

        sendRegistrationCode(newUser);
    }

    @Override
    public Page<User> getAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public Page<User> getAllUnidentified(Pageable pageable) {
        return userRepository.findAllByIdentifiedIs(false, pageable);
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public void update(int id, User user, MultipartFile multipartFile, MultipartFile multipartFileId) {
        User newUser = userRepository.getById(id);
        if(newUser.getFirstName() != null) {
            if(!multipartFileId.isEmpty()){
                String nameImage = uploadFile(multipartFileId);
                newUser.setIdentityPicture(nameImage);
                newUser.setIdentified(false);
            }
        }
        newUser.setFirstName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setPhoneNumber(user.getPhoneNumber());
        newUser.setEmail(user.getEmail());
        if(!multipartFile.isEmpty()){
            String nameImage = uploadFile(multipartFile);
            newUser.setProfilePicture(nameImage);
        }
        validateUserInput(newUser);
        userRepository.save(newUser);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public void block(User user) {
        if(user.isBlocked()){
            user.setBlocked(false);
        } else {
            user.setBlocked(true);
        }
        userRepository.save(user);
    }

    @Override
    public void identify(User user, boolean approved) {
        if(approved) {
            user.setIdentified(true);
            user.setBlocked(false);
        } else {
            user.setIdentified(false);
            user.setBlocked(true);
        }
        userRepository.save(user);
    }

    @Override
    public List<Transaction> getTransaction(User user) {
        List<Transaction> transactionList = new ArrayList<>();
        transactionList.addAll(user.getSenderTransactions());
        transactionList.addAll(user.getReceiverTransactions());
        return transactionList;
    }

    @Override
    public List<User> getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public  List<User> getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        return getByUsername(username);
    }

    @Override
    public Page<User> findUser(String search, Pageable pageable) {
        return userRepository.findAllByEmailLikeOrPhoneNumberLikeOrUsernameLike(search, search, search, pageable);
    }

    @Override
    public Page<User> findRecipient(String search, Pageable pageable) {
        return userRepository.findAllByFirstNameLikeOrLastNameLike(search, search, pageable);
    }

    @Override
    public void confirmCode(String code) {

        UserVerification token = userVerificationRepository.findByCode(code);

        if(token.getCode().equals(code)){
            token.setVerified(true);
            userVerificationRepository.save(token);
        }
        else {
            throw new IllegalArgumentException("Wrong code");
        }
    }

    @Override
    public void sendRegistrationCode(User user){
        UserVerification token = new UserVerification(user);
        token.setVerified(false);
        userVerificationRepository.save(token);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(user.getEmail());
        message.setSubject("Complete Registration!");
        message.setFrom("petyo.p.vakov@gmail.com");
        message.setText(token.getCode());
        emailSenderService.sendEmail(message);
    }

    private String uploadFile(MultipartFile file) {
        if(!file.isEmpty()){
            try {
                byte[] bytes = file.getBytes();
                String rootPath = "C:\\uploads";
                File dir = new File("C:/");
                if(!dir.exists()){
                    dir.mkdirs();
                }
                String name = String.valueOf("/uploads/"+new Date().getTime()) + ".jpg";
                File serverFile = new File(dir.getAbsolutePath()
                        +File.separator + name);
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(serverFile));
                stream.write(bytes);

                return name;
            }
            catch (IOException e){
                e.printStackTrace();
            }
        } else {
            return "/uploads/user.jpg";
        }
        return null;
    }

    private void validateUserInput(User user) {
        if (user.getUsername() == null || user.getUsername().length() < 4 || user.getUsername().length() > 25) {
            throw new InvalidUserInputException("Invalid user username");
        }
        if (user.getFirstName() == null || user.getFirstName().length() < 2 || user.getFirstName().length() > 15) {
            throw new InvalidUserInputException("Invalid user first name");
        }
        if (user.getLastName() == null || user.getLastName().length() < 4 || user.getLastName().length() > 20) {
            throw new InvalidUserInputException("Invalid user last name");
        }
    }
}
