# Virtual Wallet

Trello board link: https://trello.com/b/rM7NYZwQ

Swagger link: http://localhost:8080/swagger-ui.html/

Virtual Wallet enables users to make online payments between users. User has wallet. Each wallet has cards. Each user can make transfers to another user and make money transfers to his wallet or another user's wallet.


## 
  - Visible without authentication
    - the application start page
    - the user login form
    - the user registration form

  - When authenticated, user can see their cards and amount at wallets. Also 'Add card' button appears which allows user to add new cards, "Add funds" button which allows user to add money to wallet and "Make transaction" button wisch allows user make transactions.

  - The app have profile page that shows user’s first name, last name, email and phone number.
 
  - The admin page shows three tables - with users, transactions and identity approvals images.
### Test users

| Username | Password | Role |
| ------ | ------ | ------ |
| admin | 123123 | admin |

**Screenshots**
*  Login
![Log_in](/uploads/7d1ff70e17829345e16e6a0f41292af4/Log_in.PNG)

*  Register
![Register](/uploads/ed33858dfd74e19467f8d75b1b10eaca/Register.PNG)

*  User profile
![user_viev](/uploads/e91bf353961b032a7a9bbe497746eda4/user_viev.PNG)

*  Transaction history
![Transaction_history](/uploads/4dcb225b771fe17a7cc417fa8f58f4b9/Transaction_history.PNG)

*  Update user
![Edit_user](/uploads/74f7bf3f225f7429b949cc224644ed70/Edit_user.PNG)

*  Admin show all users
![admin_show_users](/uploads/d2ff0c06e55b8478a61652956aacf207/admin_show_users.PNG)

*  Admin show all transaction
![show_all_transaction](/uploads/db13e325d5b32b7bac49ed8ed9b58e39/show_all_transaction.PNG)

*  Admin show verify image
![admin_iamge_virify](/uploads/cf0188b51be3db45301b4a8dc28f9492/admin_iamge_virify.PNG)
